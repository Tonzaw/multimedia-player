/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.sql.*;

/**
 * DataAccessObject class
 * 
 * Handles the connection with the database
 *
 * @author Toni
 */
public class MediaAccessObject implements MediaAccessObject_IF {

    private Connection myCon;
    
    /**
     * Opens the connection to the database.
     */
    public MediaAccessObject() { // konstruktori
        try {
            Class.forName("com.mysql.jdbc.Driver");
            myCon = DriverManager.getConnection("jdbc:mysql://localhost/mediasoitin", "tonikuj", "tonikuj");
        } catch (Exception e) {
            System.err.println("Virhe tietokantayhteyden muodostamisessa.");
            System.exit(0);
        }
    }
    
    /**
     * Closes the connection to the database.
     */
    @Override
    protected void finalize() { // destruktori
        try { // sama yhteys koko sovelluksen ajan
            if (myCon != null) {
                myCon.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
