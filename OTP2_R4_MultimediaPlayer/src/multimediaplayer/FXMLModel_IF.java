/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

/**
 * Interface for the FXMLModel class
 * 
 * @author Toni
 */
public interface FXMLModel_IF {
    public abstract void addNewMusicFile();
    public abstract void addNewVideoFile();
}
