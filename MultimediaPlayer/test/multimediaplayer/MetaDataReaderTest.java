/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.Mp3File;
import java.io.File;
import java.net.URL;
import org.junit.Test;
import static org.junit.Assert.*;

public class MetaDataReaderTest {

    public MetaDataReaderTest() {

    }

    MetaDataReader reader = MetaDataReader.getInstance();

    /**
     * Test of getMusicFileData method, of class MetaDataReader.
     */
    @Test
    public void testGetMusicFileData() {

      //  Url url = new URL("/MultimediaPlayer\test\testfiles\v1tag.mp3");
        //  File file = new File (url);
        //  Url url = classfile.getResource(path);
     // File f = new File (getClass().getResource("MultimediaPlayer/test/testfiles/v23tag.mp3").getFile());
        //   reader.getMusicFileData(f);
        //    assertEquals(metadatareader.getMusicFileData(f));
    }


    /**
     * Test of findAlbum method, of class MetaDataReader.
     */
    @Test
    public void testFindAlbum() {

        
        URL url = this.getClass().getResource("/testfiles/v23tag.mp3");
        System.out.println(url);
        String file_name = url.getFile();

        File f = new File(file_name);

        MusicFile mfile = reader.getMusicFileData(f);
        String result = mfile.getAlbum();
        String expResult = "ALBUM1234567890123456789012345";
        assertEquals(expResult, result);

    }
    

    /**
     * Test of findTrackNumber method, of class MetaDataReader.
     */
    @Test
    public void testFindTrackNumber() {

        URL url = this.getClass().getResource("/testfiles/v23tag.mp3");
        String file_name = url.getFile();

        File f = new File(file_name);

        MusicFile mfile = reader.getMusicFileData(f);
        int result = mfile.getTrackNumber();
        int expResult = 1;
        assertEquals(expResult, result);

    }


    /**
     * Test of findGenre method, of class MetaDataReader.
     */
    @Test
    public void testFindGenre() {

        URL url = this.getClass().getResource("/testfiles/v23tag.mp3");
        String file_name = url.getFile();

        File f = new File(file_name);

        MusicFile mfile = reader.getMusicFileData(f);
        String result = mfile.getGenre();
        String expResult = "Pop";
        assertEquals(expResult, result);
    }

    @Test
    public void testFindGenre2() {
        URL url = this.getClass().getResource("/testfiles/v1andv23tagswithalbumimage.mp3");
        String file_name = url.getFile();

        File f = new File(file_name);

        MusicFile mfile = reader.getMusicFileData(f);
        String result = mfile.getGenre();
        String expResult = "Pop";
        assertEquals(expResult, result);

    }

    /**
     * Test of findName method, of class MetaDataReader.
     */
    @Test
    public void testFindName() {


        URL url = this.getClass().getResource("/testfiles/v23tag.mp3");
        String file_name = url.getFile();

        File f = new File(file_name);

        MusicFile mfile = reader.getMusicFileData(f);
        String result = mfile.getName();
        String expResult = "TITLE1234567890123456789012345";
        assertEquals(expResult, result);

    }



}
