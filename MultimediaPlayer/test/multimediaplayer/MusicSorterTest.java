/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Toni
 */
public class MusicSorterTest {
    
    public MusicSorterTest() {
    }
    
    /**
     * Test of getAllArtists method, of class MusicSorter.
     */
    @Test
    public void testGetAllArtists() {
        System.out.println("Testing getAllArtists method");
        
        MusicFile[] music = new MusicFile[2];
        MusicFile file1 = new MusicFile("ArtistiA", "BiisinNimiA", "ArtistinA AlbumiA", "Genre", 1, "path");
        MusicFile file2 = new MusicFile("ArtistiB", "BiisinNimiA", "ArtistinB AlbumiA", "Genre", 1, "path");
        music[0] = file1;
        music[1] = file2;
        
        List<MusicFile> musicFiles = Arrays.asList(music);
        System.out.println("Musiikki filet "+musicFiles);
        
        MusicSorter instance = new MusicSorter();
        
        ArrayList<Artist> expResult = new ArrayList<>();
        Artist artist = new Artist("ArtistiA");
        Album album = new Album("ArtistinA AlbumiA");
        MusicFile track = new MusicFile("ArtistiA", "BiisinNimiA", "ArtistinA AlbumiA", "Genre", 1, "path");
        album.addTrack(track);
        artist.addAlbum(album);
        expResult.add(artist);
        
        ArrayList<Artist> result = instance.getAllArtists(musicFiles);
        System.out.println("Result = "+ result);
        
        assertEquals(expResult.get(0).getAlbums().get(0).getTracks().get(0).getName(), result.get(0).getAlbums().get(0).getTracks().get(0).getName());
    }
    
}
