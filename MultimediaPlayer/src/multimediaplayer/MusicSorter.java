
package multimediaplayer;

import java.util.ArrayList;
import java.util.List;

/**
 * MusicSorter is a class that can only be used to change a Listof MusicFile
 * objects into an ArrayList of Artist objects. It will also link the Artist
 * objects with their albums and tracks.
 *
 * @author Toni
 */
public class MusicSorter {
    
    /**
     * Generates an ArrayList of artists with albums and tracks.
     *
     * @param musicFiles List of music file objects
     * @return ArrayList consisting of artist objects
     */
    public ArrayList<Artist> getAllArtists(List<MusicFile> musicFiles){
        ArrayList<String> artists = gatherAllArtistsToAnArrayList(musicFiles);
        ArrayList<Artist> allArtists = createArtistsWithAlbums(artists, musicFiles);
        createsTracksAndAddsThemToAlbums(allArtists, musicFiles);
        
        return allArtists;
    }
    
    /**
     * Gathers all the names of the different Artists to an AarrayList
     *
     * @param musicFiles list of music file objects
     * @return artists consisting of all artist names
     */
    private ArrayList<String> gatherAllArtistsToAnArrayList(List<MusicFile> musicFiles){
        ArrayList<String> artists = new ArrayList();
        ArrayList<String> tempArtists = new ArrayList();
        for (MusicFile musicFile : musicFiles) {
            String artist = musicFile.getArtist();
            artists.addAll(tempArtists);
            tempArtists.clear();
            if (!artists.isEmpty()) {
                boolean temp = false;
                for (String artist5 : artists) {
                    if (artist.equals(artist5)) {
                        temp = true;
                    }
                }
                if (!temp) {
                    tempArtists.add(artist);
                }
            } else {
                tempArtists.add(artist);
            }
        }
        
        artists.addAll(tempArtists);
        
        return artists;
    }
    
    /**
     * Creates artist objects with album objects
     *
     * @param artists, musicFiles Arraylist of all artist names and a list of music file objects
     * @return ArrayList consisting of all artist names
     */
    private ArrayList<Artist> createArtistsWithAlbums(ArrayList<String> artists, List<MusicFile> musicFiles){
        ArrayList<Artist> allArtists = new ArrayList();
        ArrayList<Album> allAlbums = new ArrayList();
        ArrayList<Album> tempAlbums2 = new ArrayList();
        for (String artist : artists) {
            Artist artist1 = new Artist(artist);
            for (MusicFile musicFile : musicFiles) {
                if (musicFile.getArtist().equals(artist)) {
                    allAlbums.addAll(tempAlbums2);
                    tempAlbums2.clear();
                    if (allAlbums.isEmpty()) {
                        artist1.addAlbum(new Album(musicFile.getAlbum()));
                        tempAlbums2.add(new Album(musicFile.getAlbum()));
                    } else {
                        boolean temp = false;
                        for (Album album : allAlbums) {
                            if (album.getName().equals(musicFile.getAlbum())) {
                                temp = true;
                            }
                        }
                        if (!temp) {
                            artist1.addAlbum(new Album(musicFile.getAlbum()));
                            tempAlbums2.add(new Album(musicFile.getAlbum()));
                        }
                    }
                }
            }
            allArtists.add(artist1);
        }
        
        return allArtists;
    }
    
    /**
     * Creates artist objects with album objects
     *
     * @param allArtists, musicFiles
     */
    private void createsTracksAndAddsThemToAlbums(ArrayList<Artist> allArtists, List<MusicFile> musicFiles){
        for (Artist artist : allArtists) {
            for (Album album : artist.getAlbums()) {
                for (MusicFile musicFile : musicFiles) {
                    if (musicFile.getAlbum().equals(album.getName())) {
                        album.addTrack(musicFile);
                    }
                }
            }
        }
    }
    
}
