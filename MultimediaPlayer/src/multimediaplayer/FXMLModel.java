package multimediaplayer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * FXML Model class
 *
 * @author Toni
 */
public class FXMLModel implements FXMLModel_IF {

    private final FXMLController controller;
    private final MediaAccessObject mao = new MediaAccessObject();
    private final MusicSorter musicSorter = new MusicSorter();
    private final MusicPlayer musicPlayer = new MusicPlayer(this);
    private final MetaDataReader metareader = MetaDataReader.getInstance();
    private final MusicLibraryHandler musicLibrary = new MusicLibraryHandler(musicPlayer);
    

    public FXMLModel(FXMLController controller) {
        this.controller = controller;
    }
    
    /**
     * Opens a filechooser where the user can choose music files to be added to
     * the library (not ready yet)
     */
    @Override
    public void addNewMusicFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(controller.messages.getString("selectMusicFiles"));
        fileChooser.getExtensionFilters().addAll(
                //new FileChooser.ExtensionFilter("Music Files", "*.wav", "*.mp3", "*.mp4", "*.m4a", "*.flac", "*.wma"));
                new FileChooser.ExtensionFilter(controller.messages.getString("musicFiles"), "*.mp3"));
        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null);

        if (selectedFiles != null) {
            for (int i = 0; i < selectedFiles.size(); i++) {
                File file = selectedFiles.get(i);
                TCPDataTransferer.getInstance().sendDataToServer(file);
                if (mao.addNewMusicFileToDatabase(metareader.getMusicFileData(file))) {
                    populateMusicLibrary(controller.musicLibraryTreeView);
                }
            }
        } else {

        }
    }

    /**
     * Opens a filechooser where the user can choose video files to be added to
     * the library (not ready yet)
     */
    @Override
    public void addNewVideoFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(controller.messages.getString("selectVideoFiles"));
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter(controller.messages.getString("videoFiles"), "*.avi", "*.mkv", "*.mpeg-4", "*.mov", "*.asf"));
        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null);

        if (selectedFiles != null) {

        } else {

        }
    }

    /**
     * Populates music library with all the musicfiles that are stored in the database
     *
     * @param treeView TreeView that will be populated with items
     */
    @Override
    public void populateMusicLibrary(TreeView treeView) {
        //gets all music file information from database and makes MusicFile objects
        MusicFile[] music = mao.getAllMusicFilesFromDatabase();
        List<MusicFile> musicFiles = Arrays.asList(music);

        //gets all Artist objects with their albums and tracks
        ArrayList<Artist> allArtists = musicSorter.getAllArtists(musicFiles);
        
        //Creates the whole TreeView
        musicLibrary.constructView(allArtists, treeView);
    }
    
    /**
     * Populates video library with all the videos that are stored in the database
     *
     * @param treeView TreeView that will be populated with items
     */
    @Override
    public void populateVideoLibrary(TreeView treeView) {
        //create root
        TreeItem<String> root = new TreeItem<>("Movie");
        root.setExpanded(false);
        //create child
        TreeItem<String> itemChild = new TreeItem<>("Something");
        itemChild.setExpanded(false);
        //create child
        TreeItem<String> itemChild2 = new TreeItem<>("Something");
        itemChild2.setExpanded(false);

        //root is the parent of itemChild which is the parent of itemChild2
        itemChild.getChildren().add(itemChild2);
        root.getChildren().add(itemChild);
        treeView.setRoot(root);

    }
    
    /**
     * Orders musicplayer to play the selected music file
     */
    @Override
    public void playSelectedMusicFile() {
        try {
            musicPlayer.playSelectedMusicFile();
        } catch (IOException ex) {
            Logger.getLogger(FXMLModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Orders musicplayer to pause the music
     */
    @Override
    public void pauseMusicFile() {
        musicPlayer.pauseMusicFile();
    }

    @Override
    public void setPlayingSongText(String name) {
        controller.songName.setText(name);
    }

    @Override
    public void setPlayingArtistText(String name) {
        controller.artist.setText(name);
    }
    
}
