/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.util.ArrayList;

/**
 *
 * @author Toni
 */
public class Artist implements MusicLibraryIF {
    
    private ArrayList<Album> albums = new ArrayList();
    private String name;

    public Artist(String name) {
        this.name = name;
    }

    public ArrayList<Album> getAlbums() {
        return albums;
    }
    
    public void addAlbum(Album album){
        albums.add(album);
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public String toString(){
        return name;
    }

    @Override
    public String getPath() {
        return "";
    }
    
    @Override
    public String getArtist() {
        return name;
    }
    
}
