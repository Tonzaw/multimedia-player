/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.util.HashMap;

/**
 * Interface for the MediaAccessObject class
 * 
 * @author Toni
 */
public interface MediaAccessObject_IF {
    
    public abstract boolean addNewMusicFileToDatabase(MusicFile file);
    public abstract boolean addNewVideoFileToDatabase(HashMap fileMetadata);
    public abstract MusicFile[] getAllMusicFilesFromDatabase();
    public abstract boolean getAllVideoFilesFromDatabase();
    public abstract boolean updateMusicFileById(int id);
    public abstract boolean updateVideoFileById(int id);
    public abstract boolean removeMusicFileById(int id);
    public abstract boolean removeVideoFileById(int id);
    
}
