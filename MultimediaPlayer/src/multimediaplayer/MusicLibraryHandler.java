/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

/**
 *
 * @author Toni
 */
public class MusicLibraryHandler {
    
    private final MusicPlayer musicPlayer;

    public MusicLibraryHandler(MusicPlayer musicPlayer) {
        this.musicPlayer = musicPlayer;
    }
    
    public void constructView(ArrayList<Artist> allArtists, TreeView treeView) {
        //create root item
        TreeItem<MusicLibraryIF> root = new TreeItem<>(new Artist("Music"));
        root.setExpanded(true);

        //Populates the TreeView with all the artists, albums and tracks
        for (Artist artist : allArtists) {
            TreeItem<MusicLibraryIF> artistItem = new TreeItem<>(artist);
            root.getChildren().add(artistItem);
            for (Album album : artist.getAlbums()) {
                TreeItem<MusicLibraryIF> albumItem = new TreeItem<>(album);
                artistItem.getChildren().add(albumItem);
                for (MusicFile track : album.getTracks()) {
                    TreeItem<MusicLibraryIF> trackItem = new TreeItem<>(track);
                    albumItem.getChildren().add(trackItem);
                }
            }
        }
        
        //keeps updating what musicfile is selected
        treeView.getSelectionModel().selectedItemProperty()
                .addListener(new ChangeListener<TreeItem<MusicLibraryIF>>() {

                    @Override
                    public void changed(
                            ObservableValue<? extends TreeItem<MusicLibraryIF>> observable,
                            TreeItem<MusicLibraryIF> old_val, TreeItem<MusicLibraryIF> new_val) {
                                TreeItem<MusicLibraryIF> selectedItem = new_val;
                                System.out.println("Path selected: "+ selectedItem.getValue().getPath());
                                if (!"".equals(selectedItem.getValue().getPath())){
                                    musicPlayer.playingMusicFileName = selectedItem.getValue().getName();
                                    musicPlayer.playingArtistName = selectedItem.getValue().getArtist();
                                    musicPlayer.selectedMusicFilePath = selectedItem.getValue().getPath();
                                }
                            }

                });

        //sets the root which is the parent of all others
        treeView.setRoot(root);
    }
    
}
