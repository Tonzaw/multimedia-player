/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * Class that controls all the actions related to playing music
 *
 * @author Toni
 */
public class MusicPlayer {

    private final FXMLModel model;
    private MediaPlayer player;
    public String selectedMusicFilePath = "";
    public String playingMusicFilePath = "";
    public String playingMusicFileName = "";
    public String playingArtistName = "";
    private Thread streamReader;
    private MediaStreamReader mediaStreamReader;

    public MusicPlayer(FXMLModel model) {
        this.model = model;
    }

    /**
     * Sets the media player with a song and keeps record of what song is
     * playing or paused
     */
    public void setMediaPlayer() throws IOException {

        if (!"".equals(selectedMusicFilePath)) {
            MediaHandler mediaHandler = new MediaHandler();
            mediaStreamReader = new MediaStreamReader(selectedMusicFilePath, mediaHandler);
            if (streamReader != null) {
                if (streamReader.isAlive()) {
                    System.out.println("is alive here");
                    mediaStreamReader.stopThread();
                }
            }
                streamReader = new Thread(mediaStreamReader);
                streamReader.start();
 

            //Media media = new Media(new File(selectedMusicFilePath).toURI().toString());
            Media media = null;
            /*while(!mediaHandler.hasDownloadedEnough()){
            }*/
            media = new Media(mediaHandler.getMediaFile().toURI().toString());

            player = new MediaPlayer(media);
            player.play();
            playingMusicFilePath = selectedMusicFilePath;
            model.setPlayingSongText(playingMusicFileName);
            model.setPlayingArtistText(playingArtistName);
        }
    }

    /**
     * Starts playing the selected music file. If it is paused, continues from
     * the same time it was paused.
     */
    public void playSelectedMusicFile() throws IOException {
        if (player != null) {
            if (player.getStatus().equals(MediaPlayer.Status.PLAYING)) {
                if (!playingMusicFilePath.equals(selectedMusicFilePath)) {
                    player.stop();
                    setMediaPlayer();
                }
            } else if (player.getStatus().equals(MediaPlayer.Status.PAUSED)) {
                if (playingMusicFilePath.equals(selectedMusicFilePath)) {
                    player.play();
                } else {
                    setMediaPlayer();
                }
            }
        } else {
            setMediaPlayer();
        }
    }

    /**
     * Pauses the music
     */
    public void pauseMusicFile() {
        player.pause();
    }

}
