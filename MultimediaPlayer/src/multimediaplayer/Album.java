/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.util.ArrayList;

/**
 *
 * @author Toni
 */
public class Album implements MusicLibraryIF {

    private ArrayList<MusicFile> tracks = new ArrayList();
    private String name;

    public Album(String name) {
        this.name = name;
    }

    public ArrayList<MusicFile> getTracks() {
        return tracks;
    }

    public void addTrack(MusicFile track) {
        tracks.add(track);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getPath() {
        return "";
    }

    @Override
    public String getArtist() {
        return "";
    }
}
