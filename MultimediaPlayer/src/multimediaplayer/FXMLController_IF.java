/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

/**
 * Interface for the FXMLController class
 *
 * @author Toni
 */
public interface FXMLController_IF {
    public abstract void addNewMusicFile();
    public abstract void addNewVideoFile();
    public abstract void populateMusicLibrary();
    public abstract void populateVideoLibrary();
    public abstract void playSelectedMusicFile();
    public abstract void pauseMusicFile();
    public abstract void close();
    public abstract void changeLanguage();
    public abstract void selectSwedish();
    public abstract void selectEnglish();
}
