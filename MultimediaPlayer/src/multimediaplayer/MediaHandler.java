/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaplayer;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is a Media handler class.
 *
 * @author Sami
 */
public class MediaHandler {

    private RandomAccessFile rw;
    private File tempFile;
    private byte[] data;
    private float downloadPercent;
    private long length;

    /**
     * Public class constructor.
     */
    public MediaHandler() throws IOException {
        tempFile = File.createTempFile("Media", ".mp3");
        rw = new RandomAccessFile(tempFile, "rw");
    }

    /**
     * Sets the cursor to a point of file where next read or write occurs.
     *
     * @param current_file_pointer a current offset of this file
     */
    public void seek(long current_file_pointer) throws IOException {
        rw.seek(current_file_pointer);
    }

    /**
     * Writes bytes from a specific array starting from the current file
     * pointer.
     *
     * @param data data in a byte array
     */
    public void write(byte[] data) throws IOException {
        rw.write(data);
    }

    /**
     * Returns the offset from the beginning of the file, in bytes, at which the
     * next read or write occurs.
     *
     * @return returns the file pointer
     */
    public long getFilePointer() throws IOException {
        return rw.getFilePointer();
    }

    /**
     * Returns the random access file.
     */
    public RandomAccessFile getMedia() {
        return rw;
    }

    /**
     * Returns the temporary file.
     */
    public File getMediaFile() {
        return tempFile;
    }

    /**
     * Sets length for the file before it's fully downloaded.
     *
     * @param length length of the whole file
     */
    public void setLength(long length) {
        this.length = length;
    }

    /**
     * Returns the total length of the file.
     */
    public long getLength() {
        return length;
    }

    /**
     * Measures how much the file has downloaded and sets the download percent.
     *
     * @param filepointer a current offset of this file
     */
    public void setDownloadPercent(float filepointer) {
        downloadPercent = ((float) (filepointer / length * 100));
        System.out.println("Downloaded: " + downloadPercent + "%");
    }

    /**
     * Checks if download percent is high enough and returns true else it returns false.
     *
     * @return returns a true when file has downloaded enough or false when it has not downloaded enough
     */
    public boolean hasDownloadedEnough() {
        if (downloadPercent >= 100) {
            System.out.println("Should play %: "+downloadPercent);
            return true;
        }
        return false;
    }

}
