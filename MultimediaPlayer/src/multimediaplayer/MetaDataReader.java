package multimediaplayer;

import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is a metadata reader class.
 * 
 * @author Sami
 */
public class MetaDataReader {

    private final EnumMap<ID3Tag, ID3v1> ID3TagMap
            = new EnumMap<ID3Tag, ID3v1>(ID3Tag.class);

    private final Set<String> bannedWords = new HashSet<String>(Arrays.asList("   ", " ", "", "CurseWord1"));

    private Mp3File mp3file;

    private MusicFile musicFile;

    private static MetaDataReader INSTANCE = null;
    
    /**
     * Returns an instance of this class.
     * This method creates an instance of this class if it doesn't already exist.
     *
     * @return returns the instance of this class 
     */
    public static MetaDataReader getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MetaDataReader();
        }
        return INSTANCE;
    }

    /**
     * Empty private class constructor.
     */
    private MetaDataReader() {

    }
    
    /**
     * Returns a MusicFile object that includes metadata from the music file.
     * Creates a MusicFile object and reads metadata from file given in parameter to the MusicFile object.
     *
     * @param file music file where metadata is read
     * @return returns a MusicFile Object
     */
    public MusicFile getMusicFileData(File file) {
        try {
            
            this.mp3file = new Mp3File(file);
            findTag(mp3file);
            musicFile = new MusicFile(findArtist(), findName(), findAlbum(), findGenre(), findTrackNumber(), getPath(file));
        } catch (IOException ex) {
            Logger.getLogger(MetaDataReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedTagException ex) {
            Logger.getLogger(MetaDataReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidDataException ex) {
            Logger.getLogger(MetaDataReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return musicFile;
    }
    
    /**
     * Reads artist's name from metadata and returns it.
     *
     * @return returns an artist name
     */
    public String findArtist() {
        if (ID3TagMap.get(ID3Tag.id3v1Tag) != null) {
            if (!bannedWords.contains(ID3TagMap.get(ID3Tag.id3v1Tag).getArtist())
                    && ID3TagMap.get(ID3Tag.id3v1Tag).getArtist() != null) {
                return ID3TagMap.get(ID3Tag.id3v1Tag).getArtist();
            }
        } else if (ID3TagMap.get(ID3Tag.id3v2Tag) != null) {
            if (!bannedWords.contains(((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getAlbumArtist())
                    && ((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getAlbumArtist() != null) {
                return ((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getAlbumArtist();
            }
        }
        return "Unknown";
    }

    /**
     * Reads album's name from metadata and returns it.
     *
     * @return returns an album name
     */
    public String findAlbum() {

        if (ID3TagMap.get(ID3Tag.id3v1Tag) != null) {
            if (!bannedWords.contains(ID3TagMap.get(ID3Tag.id3v1Tag).getAlbum())
                    && ID3TagMap.get(ID3Tag.id3v1Tag).getAlbum() != null) {
                return ID3TagMap.get(ID3Tag.id3v1Tag).getAlbum();
            }
        } else if (ID3TagMap.get(ID3Tag.id3v2Tag) != null) {
            if (!bannedWords.contains(((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getAlbum())
                    && ((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getAlbum() != null) {
                return ((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getAlbum();
                
                
            }
        }
        return "Unknown";
        
    }

    /**
     * Reads track number from metadata and returns it.
     *
     * @return returns a track number
     */
    public int findTrackNumber() {
        if (ID3TagMap.get(ID3Tag.id3v1Tag) != null) {
            if (!bannedWords.contains(ID3TagMap.get(ID3Tag.id3v1Tag).getTrack())
                    && ID3TagMap.get(ID3Tag.id3v1Tag).getTrack() != null) {
                return Integer.parseInt(ID3TagMap.get(ID3Tag.id3v1Tag).getTrack());
            }
        } else if (ID3TagMap.get(ID3Tag.id3v2Tag) != null) {
            if (!bannedWords.contains(((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getTrack())
                    && ((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getTrack() != null) {
                return Integer.parseInt(((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getTrack());
            }
        }
        return 0;

    }

    /**
     * Reads genre from metadata and returns it.
     *
     * @return returns a genre
     */
    public String findGenre() {
        if (ID3TagMap.get(ID3Tag.id3v1Tag) != null) {
            if (!bannedWords.contains(ID3TagMap.get(ID3Tag.id3v1Tag).getGenreDescription())
                    && ID3TagMap.get(ID3Tag.id3v1Tag).getGenreDescription() != null) {
                return ID3TagMap.get(ID3Tag.id3v1Tag).getGenreDescription();
            }
        } else if (ID3TagMap.get(ID3Tag.id3v2Tag) != null) {
            if (!bannedWords.contains(((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getGenreDescription())
                    && ((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getGenreDescription() != null) {
                return ((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getGenreDescription();
            }
        }
        return "Unknown";

    }

    /**
     * Reads name of the track from metadata and returns it.
     *
     * @return returns name of the track
     */
    public String findName() {
        if (ID3TagMap.get(ID3Tag.id3v1Tag) != null) {
            if (!bannedWords.contains(ID3TagMap.get(ID3Tag.id3v1Tag).getTitle())
                    && ID3TagMap.get(ID3Tag.id3v1Tag).getTitle() != null) {
                return ID3TagMap.get(ID3Tag.id3v1Tag).getTitle();
            }
        } else if (ID3TagMap.get(ID3Tag.id3v2Tag) != null) {
            if (!bannedWords.contains(((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getTitle())
                    && ((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getTitle() != null) {
                return ((ID3v2)(ID3TagMap.get(ID3Tag.id3v2Tag))).getTitle();
            }
        }
        return "Unknown";
    }

    /**
     * Finds right ID3 tag from music file.
     * Music file must have ID3 tag or else tag is saved as null.
     *
     * @param file Music file where ID3 tag is read
     */
    public void findTag(Mp3File file) {
        if (file.hasId3v1Tag()) {
            ID3TagMap.put(ID3Tag.id3v1Tag, getID3V1Tag(file));
        } else if (file.hasId3v2Tag()) {
            ID3TagMap.put(ID3Tag.id3v2Tag, getID3V2Tag(file));
        }else{
            ID3TagMap.put(ID3Tag.id3v1Tag, null);
            ID3TagMap.put(ID3Tag.id3v2Tag, null);
        }
    }
    
    public String getPath(File file){
        String path = "/1/" + file.getName(); 
        return path;
    }

    /**
     * Reads ID3v1 tag from the music file.
     *
     * @param file Music file where ID3 tag is read
     * @return The ID3v1 tag from the file
     */
    public ID3v1 getID3V1Tag(Mp3File file) {
        if (file != null) {
            return file.getId3v1Tag();
        }
        return null;
    }

    /**
     * Reads ID3v2 tag from the music file.
     *
     * @param file Music file where ID3 tag is read
     * @return The ID3v2 tag from the file
     */
    public ID3v2 getID3V2Tag(Mp3File file) {
        if (file != null) {
            return file.getId3v2Tag();
        }
        return null;
    }

}
