package multimediaplayer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sami
 */
public class MediaStreamReader implements Runnable {

    private Socket target_socket;
    private DataInputStream din;
    private DataOutputStream dout;
    private String path;
    MediaHandler mediaHandler;
    private boolean running = true;

    long current_file_pointer;

    /**
     * Public constructor of this class.
     * 
     * @param path path where file if found on the server side
     * @param mediaHandler media handler that handles the incoming data from server and puts the file together.
     */
    public MediaStreamReader(String path, MediaHandler mediaHandler) {
        try {
            target_socket = new Socket(InetAddress.getByName("127.0.0.1"), 6578);
            this.path = path;
            this.mediaHandler = mediaHandler;
            din = new DataInputStream(target_socket.getInputStream());
            dout = new DataOutputStream(target_socket.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger(MediaStreamReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * A loop where incoming packets are handled.
     */
    //@Override
    public void run() {
        // rw = null;
        current_file_pointer = 0;
        boolean loop_break = false;

        try {
            dout.write(createDataPacket("224".getBytes("UTF8"), path.getBytes("UTF8")));
            dout.flush();
            System.out.println("MediaStreamReader - lähetetty path serverille");
        } catch (Exception e) {
            e.printStackTrace();
        }

        while (running) {
            byte[] initilize = new byte[1];
            try {
                din.read(initilize, 0, initilize.length);
                if (initilize[0] == 2) {
                    byte[] cmd_buff = new byte[3];
                    din.read(cmd_buff, 0, cmd_buff.length);
                    byte[] recv_data = readStream();
                    switch (Integer.parseInt(new String(cmd_buff))) {
                        case 224:
                            mediaHandler.setLength(Long.parseLong(new String(recv_data)));
                            dout.write(createDataPacket("226".getBytes("UTF8"), String.valueOf(current_file_pointer).getBytes("UTF8")));
                            dout.flush();
                            break;
                        case 226:
                            System.out.println("Kuseee");
                            mediaHandler.seek(current_file_pointer);
                            mediaHandler.write(recv_data);
                            current_file_pointer = mediaHandler.getFilePointer();
                            mediaHandler.setDownloadPercent(current_file_pointer);
                            dout.write(createDataPacket("226".getBytes("UTF8"), String.valueOf(current_file_pointer).getBytes("UTF8")));
                            dout.flush();
                            break;
                        case 227:
                            if ("Close".equals(new String(recv_data))) {
                                loop_break = true;
                                System.out.println("Suljettu");
                            }
                            break;
                    }
                }
                if (loop_break == true) {
                    target_socket.close();
                    running = false;

                }
            } catch (IOException ex) {
                Logger.getLogger(MediaStreamReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (!target_socket.isClosed()){
            try {
                target_socket.close();
            } catch (IOException ex) {
                Logger.getLogger(MediaStreamReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Reads data from packet. This method is used to read actual data from
     * incoming packet stream
     *
     * @return data buffer
     */
    public byte[] readStream() {
        byte[] data_buff = null;
        try {
            int b = 0;
            String buff_length = "";
            while ((b = din.read()) != 4) {
                buff_length += (char) b;
            }
            int data_length = Integer.parseInt(buff_length);
            data_buff = new byte[Integer.parseInt(buff_length)];
            int byte_read = 0;
            int byte_offset = 0;
            while (byte_offset < data_length) {
                byte_read = din.read(data_buff, byte_offset, data_length - byte_offset);
                byte_offset += byte_read;

            }
        } catch (IOException ex) {
            Logger.getLogger(MediaStreamReader.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return data_buff;
    }

    /**
     * Creates a packet and it includes command, lenght of data and actual data.
     * This method creates byte array and copies command, lenght of data and
     * actual data to it
     *
     * @param cmd command that is included in packet
     * @param data data that is included in packet
     * @return returns a packet
     */
    public byte[] createDataPacket(byte[] cmd, byte[] data) {
        byte[] packet = null;
        try {
            byte[] initialize = new byte[1];
            initialize[0] = 2;
            byte[] separator = new byte[1];
            separator[0] = 4;
            byte[] data_length = String.valueOf(data.length).getBytes("UTF8");
            packet = new byte[initialize.length + cmd.length + separator.length + data_length.length + data.length];
            System.arraycopy(initialize, 0, packet, 0, initialize.length);
            System.arraycopy(cmd, 0, packet, initialize.length, cmd.length);
            System.arraycopy(data_length, 0, packet, initialize.length + cmd.length, data_length.length);
            System.arraycopy(separator, 0, packet, initialize.length + cmd.length + data_length.length, separator.length);
            System.arraycopy(data, 0, packet, initialize.length + cmd.length + data_length.length + separator.length, data.length);

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(MediaStreamReader.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return packet;
    }
    
    public void stopThread(){
        running = false;
    }
}
